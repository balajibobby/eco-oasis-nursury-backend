package com.ts.nursery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.ShoppingCartDAO;
import com.model.Accessories;
import com.model.Fertilizers;
import com.model.Pebbles;
import com.model.Plants;
import com.model.Pots;
import com.model.Seeds;
@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/api/cart")
public class ShoppingCartController {
    private final ShoppingCartDAO shoppingCartDAO;

    @Autowired
    public ShoppingCartController(ShoppingCartDAO shoppingCartDAO) {
        this.shoppingCartDAO = shoppingCartDAO;
    }

    // Add item to cart endpoints
    @PostMapping("/add/plant")
    public void addToCartPlant(@RequestBody Plants plant) {
        shoppingCartDAO.editPlant(plant);
    }

    @PostMapping("/add/seed")
    public void addToCartSeed(@RequestBody Seeds seed) {
        shoppingCartDAO.editSeed(seed);
    }

    @PostMapping("/add/pebble")
    public void addToCartPebble(@RequestBody Pebbles pebble) {
        shoppingCartDAO.editPebble(pebble);
    }

    @PostMapping("/add/fertilizer")
    public void addToCartFertilizer(@RequestBody Fertilizers fertilizer) {
        shoppingCartDAO.editFertilizer(fertilizer);
    }

    @PostMapping("/add/accessory")
    public void addToCartAccessory(@RequestBody Accessories accessory) {
        shoppingCartDAO.editAccessory(accessory);
    }

    @PostMapping("/add/pot")
    public void addToCartPot(@RequestBody Pots pot) {
        shoppingCartDAO.editPot(pot);
    }

    // Delete item from cart endpoints
    @DeleteMapping("/delete/plant/{id}")
    public void deletePlant(@PathVariable int id) {
        shoppingCartDAO.deletePlant(id);
    }

    @DeleteMapping("/delete/seed/{id}")
    public void deleteSeed(@PathVariable int id) {
        shoppingCartDAO.deleteSeed(id);
    }

    @DeleteMapping("/delete/pebble/{id}")
    public void deletePebble(@PathVariable int id) {
        shoppingCartDAO.deletePebble(id);
    }

    @DeleteMapping("/delete/fertilizer/{id}")
    public void deleteFertilizer(@PathVariable int id) {
        shoppingCartDAO.deleteFertilizer(id);
    }

    @DeleteMapping("/delete/accessory/{id}")
    public void deleteAccessory(@PathVariable int id) {
        shoppingCartDAO.deleteAccessory(id);
    }

    @DeleteMapping("/delete/pot/{id}")
    public void deletePot(@PathVariable int id) {
        shoppingCartDAO.deletePot(id);
    }
}
